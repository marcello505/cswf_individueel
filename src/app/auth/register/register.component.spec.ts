import { HttpClient } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AuthServiceService } from '../auth-service.service';
import { MockModule, MockProvider } from 'ng-mocks';

import { RegisterComponent } from './register.component';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

describe('RegisterComponent', () => {
  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;
  let httpServiceSpy: jasmine.SpyObj<HttpClient>;

  beforeEach(async () => {
    const httpSpy = jasmine.createSpyObj('HttpClient', [
      'post',
      'get',
      'put',
      'delete',
    ]);
    await TestBed.configureTestingModule({
      declarations: [RegisterComponent],
      providers: [
        { provide: HttpClient, useValue: httpSpy },
        MockProvider(AuthServiceService),
        FormBuilder,
        MockProvider(FormGroup),
        MockProvider(Router),
        MockProvider(ActivatedRoute),
      ],
    }).compileComponents();
    httpServiceSpy = TestBed.inject(HttpClient) as jasmine.SpyObj<HttpClient>;
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

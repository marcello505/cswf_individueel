import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { MockModule, MockProvider } from 'ng-mocks';

import { AccountGuard } from './account.guard';
import { AuthServiceService } from './auth-service.service';

describe('AccountGuard', () => {
  let guard: AccountGuard;
  let httpServiceSpy: jasmine.SpyObj<HttpClient>;

  beforeEach(() => {
    const httpSpy = jasmine.createSpyObj('HttpClient', [
      'post',
      'get',
      'put',
      'delete',
    ]);
    TestBed.configureTestingModule({
      providers: [
        MockProvider(Router),
        MockProvider(AuthServiceService),
        { provide: HttpClient, useValue: httpSpy },
      ],
    });
    guard = TestBed.inject(AccountGuard);
    httpServiceSpy = TestBed.inject(HttpClient) as jasmine.SpyObj<HttpClient>;
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});

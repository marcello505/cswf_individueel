import { HttpClient } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MockModule, MockProvider } from 'ng-mocks';
import { AuthServiceService } from '../auth-service.service';

import { LoginComponent } from './login.component';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let httpServiceSpy: jasmine.SpyObj<HttpClient>;

  beforeEach(async () => {
    const httpSpy = jasmine.createSpyObj('HttpClient', [
      'post',
      'get',
      'put',
      'delete',
    ]);
    await TestBed.configureTestingModule({
      declarations: [LoginComponent],
      providers: [
        { provide: HttpClient, useValue: httpSpy },
        MockProvider(AuthServiceService),
        FormBuilder,
        MockProvider(FormGroup),
        MockProvider(Router),
        MockProvider(ActivatedRoute),
      ],
    }).compileComponents();
    httpServiceSpy = TestBed.inject(HttpClient) as jasmine.SpyObj<HttpClient>;
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {
  HttpClient,
  HttpEvent,
  HttpHandler,
  HttpHeaders,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ErrorHandlerService } from '../error-handler.service';

@Injectable({
  providedIn: 'root',
})
export class AuthServiceService {
  private authUrl = environment.apiUrl + '/auth';
  private httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };
  constructor(
    private http: HttpClient,
    private errorHandlerService: ErrorHandlerService,
    private router: Router
  ) {}

  login(account: Account): Observable<String> {
    return this.http
      .post(this.authUrl + '/login', account, this.httpOptions)
      .pipe(
        map((rawResult: Object) => {
          localStorage.setItem('account_token', rawResult['token']);
          localStorage.setItem('account_id', rawResult['account']._id);
          return rawResult['account']._id;
        }),
        catchError(this.errorHandlerService.handleError<Account>('login'))
      );
  }

  register(account: Account): Observable<String> {
    return this.http
      .post(this.authUrl + '/register', account, this.httpOptions)
      .pipe(
        map((rawResult) => {
          return rawResult['status'];
        }),
        catchError(this.errorHandlerService.handleError<Account>('register'))
      );
  }

  verify(): Observable<boolean | UrlTree> {
    return this.http.get(this.authUrl + '/verify', this.httpOptions).pipe(
      map((rawResult) => {
        if (rawResult['verified']) {
          return true;
        } else {
          localStorage.setItem('account_token', '');
          localStorage.setItem('account_id', '');
          return this.router.parseUrl('/auth/login');
        }
      }),
      catchError(this.errorHandlerService.handleError<boolean>('verify'))
    );
  }
}

@Injectable({
  providedIn: 'root',
})
export class AuthInterceptor implements HttpInterceptor {
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const idToken = localStorage.getItem('account_token');
    if (idToken) {
      const cloned = req.clone({
        headers: req.headers.set('Authorization', 'Bearer ' + idToken),
      });

      return next.handle(cloned);
    } else {
      return next.handle(req);
    }
  }
}

export interface Account {
  email: String;
  password: String;
}

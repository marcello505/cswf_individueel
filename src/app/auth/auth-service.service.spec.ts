import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { ErrorHandlerService } from '../error-handler.service';

import { AuthServiceService } from './auth-service.service';
import { MockProvider } from 'ng-mocks';
import { Router } from '@angular/router';

describe('AuthServiceService', () => {
  let service: AuthServiceService;
  let httpServiceSpy: jasmine.SpyObj<HttpClient>;

  beforeEach(() => {
    const httpSpy = jasmine.createSpyObj('HttpClient', [
      'post',
      'get',
      'put',
      'delete',
    ]);
    const errorHandlerSpy = jasmine.createSpyObj('ErrorHandlerService', [
      'handleError',
    ]);

    TestBed.configureTestingModule({
      providers: [
        { provide: HttpClient, useValue: httpSpy },
        { provide: ErrorHandlerService, useValue: errorHandlerSpy },
        MockProvider(Router),
      ],
    });
    service = TestBed.inject(AuthServiceService);
    httpServiceSpy = TestBed.inject(HttpClient) as jasmine.SpyObj<HttpClient>;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AboutComponent } from './about/about.component';
import { UsecaseComponent } from './about/usecase/usecase.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HomeComponent } from './home/home.component';
import { ErrorComponent } from './error/error.component';
import { BookDetailsComponent } from './book/book-details/book-details.component';
import { BookEditComponent } from './book/book-edit/book-edit.component';
import { BookCreateComponent } from './book/book-create/book-create.component';
import { BookFormComponent } from './book/book-form/book-form.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { BookComponent } from './book/book.component';
import { BookListComponent } from './book/book-list/book-list.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { BookDeleteComponent } from './book/book-delete/book-delete.component';
import { AuthInterceptor } from './auth/auth-service.service';
import { WriterListComponent } from './writer/writer-list/writer-list.component';
import { WriterComponent } from './writer/writer.component';
import { MatCardModule } from '@angular/material/card';
import { WriterDetailsComponent } from './writer/writer-details/writer-details.component';
import { WriterFormComponent } from './writer/writer-form/writer-form.component';
import { WriterEditComponent } from './writer/writer-edit/writer-edit.component';
import { WriterCreateComponent } from './writer/writer-create/writer-create.component';
import { WriterDeleteComponent } from './writer/writer-delete/writer-delete.component';
import { LogoutComponent } from './auth/logout/logout.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { ProfileComponent } from './profile/profile.component';
import { ProfileListComponent } from './profile/profile-list/profile-list.component';
import { ProfileDetailsComponent } from './profile/profile-details/profile-details.component';
import { ProfileFormComponent } from './profile/profile-form/profile-form.component';
import { ProfileEditComponent } from './profile/profile-edit/profile-edit.component';
import { ProfileDeleteComponent } from './profile/profile-delete/profile-delete.component';
import { BookCopyComponent } from './book-copy/book-copy.component';
import { BookCopyListComponent } from './book-copy/book-copy-list/book-copy-list.component';
import { BookCopyFormComponent } from './book-copy/book-copy-form/book-copy-form.component';
import { BookCopyDetailsComponent } from './book-copy/book-copy-details/book-copy-details.component';
import { BookCopyEditComponent } from './book-copy/book-copy-edit/book-copy-edit.component';
import { BookCopyCreateComponent } from './book-copy/book-copy-create/book-copy-create.component';
import { BookCopyDeleteComponent } from './book-copy/book-copy-delete/book-copy-delete.component';
import { MatCheckboxModule } from '@angular/material/checkbox';

@NgModule({
  declarations: [
    AppComponent,
    AboutComponent,
    UsecaseComponent,
    NavbarComponent,
    HomeComponent,
    ErrorComponent,
    BookDetailsComponent,
    BookEditComponent,
    BookCreateComponent,
    BookComponent,
    BookListComponent,
    BookFormComponent,
    BookDeleteComponent,
    WriterListComponent,
    WriterComponent,
    WriterDetailsComponent,
    WriterFormComponent,
    WriterEditComponent,
    WriterCreateComponent,
    WriterDeleteComponent,
    LogoutComponent,
    LoginComponent,
    RegisterComponent,
    ProfileComponent,
    ProfileListComponent,
    ProfileDetailsComponent,
    ProfileFormComponent,
    ProfileEditComponent,
    ProfileDeleteComponent,
    BookCopyComponent,
    BookCopyListComponent,
    BookCopyFormComponent,
    BookCopyDetailsComponent,
    BookCopyEditComponent,
    BookCopyCreateComponent,
    BookCopyDeleteComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    HttpClientModule,
    MatSnackBarModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCardModule,
    MatCheckboxModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent implements OnInit {
  private subscriptions: Subscription[] = [];
  title: String;
  loggedIn = false;
  loggedInId = '';

  constructor(private appComponent: AppComponent, private router: Router) {}

  ngOnInit(): void {
    this.title = this.appComponent.title;
    this.subscriptions.push(
      this.router.events.subscribe((event) => {
        this.checkIfLoggedIn();
      })
    );
  }

  checkIfLoggedIn(): void {
    const account_token = localStorage.getItem('account_token');
    if (account_token == null || account_token == undefined) {
      // Prevent these being null
      localStorage.setItem('account_token', '');
      localStorage.setItem('account_id', '');
    }
    if (account_token && account_token.length > 0) {
      this.loggedIn = true;
      this.loggedInId = localStorage.getItem('account_id');
    } else {
      this.loggedIn = false;
    }
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MockModule, MockProvider } from 'ng-mocks';
import { AppComponent } from '../app.component';
import { NavbarComponent } from './navbar.component';

describe('NavbarComponent', () => {
  let component = true;
  let fixture: ComponentFixture<NavbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NavbarComponent],
      providers: [MockProvider(AppComponent)],
    }).compileComponents();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

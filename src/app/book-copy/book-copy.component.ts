import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BookCopy } from './book-copy.service';

@Component({
  selector: 'app-book-copy',
  templateUrl: './book-copy.component.html',
  styleUrls: ['./book-copy.component.css'],
})
export class BookCopyComponent implements OnInit {
  constructor(private router: Router) {}

  ngOnInit(): void {}

  navigateToBookCopy(bookCopy: BookCopy) {
    this.router.navigateByUrl(`/bookcopy/details/${bookCopy._id}`);
  }
}

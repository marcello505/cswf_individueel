import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookCopyDetailsComponent } from './book-copy-details.component';

describe('BookCopyDetailsComponent', () => {
  let component = true;
  let fixture: ComponentFixture<BookCopyDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BookCopyDetailsComponent],
    }).compileComponents();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

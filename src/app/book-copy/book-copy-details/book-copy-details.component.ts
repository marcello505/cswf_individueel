import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { BookCopy, BookCopyService } from '../book-copy.service';

@Component({
  selector: 'app-book-copy-details',
  templateUrl: './book-copy-details.component.html',
  styleUrls: ['./book-copy-details.component.css'],
})
export class BookCopyDetailsComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  bookCopy: BookCopy;

  loggedIn = localStorage.getItem('account_id').length > 0;
  isLoanable = false;
  isLoaned = false;
  isLoanedToUser = false;

  constructor(
    private bookCopyService: BookCopyService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    const bookCopyId = this.route.snapshot.paramMap.get('id');
    this.bookCopyService.getById(bookCopyId).subscribe((result) => {
      this.bookCopy = result;
      if (this.bookCopy == undefined) {
        this.router.navigateByUrl('/404');
      } else {
        // Check loaned status
        this.isLoanable = this.bookCopy.canBeLoaned;
        this.isLoaned = this.bookCopy.isCurrentlyLoaned;
        this.isLoanedToUser = this.bookCopy.isCurrentlyLoaned
          ? this.bookCopy.loanedTo == localStorage.getItem('account_id')
          : false;
      }
    });
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  toggleLoan(): void {
    if (!this.isLoanedToUser) {
      this.subscriptions.push(
        this.bookCopyService
          .loanBookCopy(this.bookCopy._id)
          .subscribe((result) => {
            this.isLoaned = true;
            this.isLoanedToUser = true;
          })
      );
    } else {
      this.subscriptions.push(
        this.bookCopyService
          .unloanBookCopy(this.bookCopy._id)
          .subscribe((result) => {
            this.isLoaned = false;
            this.isLoanedToUser = false;
          })
      );
    }
  }
}

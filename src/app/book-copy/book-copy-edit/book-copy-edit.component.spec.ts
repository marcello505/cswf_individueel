import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookCopyEditComponent } from './book-copy-edit.component';

describe('BookCopyEditComponent', () => {
  let component = true;
  let fixture: ComponentFixture<BookCopyEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BookCopyEditComponent],
    }).compileComponents();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

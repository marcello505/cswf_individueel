import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { BookCopy, BookCopyService } from '../book-copy.service';

@Component({
  selector: 'app-book-copy-edit',
  templateUrl: './book-copy-edit.component.html',
  styleUrls: ['./book-copy-edit.component.css'],
})
export class BookCopyEditComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  bookCopy: BookCopy;

  constructor(
    private route: ActivatedRoute,
    private bookCopyService: BookCopyService
  ) {}

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.subscriptions.push(
      this.bookCopyService
        .getById(id)
        .subscribe((result) => (this.bookCopy = result))
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}

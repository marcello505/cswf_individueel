import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { BookCopy, BookCopyService } from '../book-copy.service';

@Component({
  selector: 'app-book-copy-delete',
  templateUrl: './book-copy-delete.component.html',
  styleUrls: ['./book-copy-delete.component.css'],
})
export class BookCopyDeleteComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  bookCopy: BookCopy;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private bookCopyService: BookCopyService
  ) {}

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.subscriptions.push(
      this.bookCopyService
        .getById(id)
        .subscribe((result) => (this.bookCopy = result))
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  deleteBookCopy() {
    this.subscriptions.push(
      this.bookCopyService
        .deleteBookCopy(this.bookCopy._id)
        .subscribe(() => this.router.navigateByUrl('/bookcopy'))
    );
  }

  cancelDelete() {
    this.router.navigateByUrl(`/bookcopy/details/${this.bookCopy._id}`);
  }
}

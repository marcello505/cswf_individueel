import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookCopyDeleteComponent } from './book-copy-delete.component';

describe('BookCopyDeleteComponent', () => {
  let component = true;
  let fixture: ComponentFixture<BookCopyDeleteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BookCopyDeleteComponent],
    }).compileComponents();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

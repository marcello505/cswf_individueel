import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { Book } from 'src/app/book/book.service';
import { BookCopy, BookCopyService } from '../book-copy.service';

@Component({
  selector: 'app-book-copy-form',
  templateUrl: './book-copy-form.component.html',
  styleUrls: ['./book-copy-form.component.css'],
})
export class BookCopyFormComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  @Input() bookCopy: BookCopy;
  @Input() readOnly = false;
  bookCopyForm: FormGroup;

  // Modal vars
  closeResult = '';

  // Form state
  isAnEdit = false;
  editId: String;
  loading = false;

  constructor(
    private bookCopyService: BookCopyService,
    private formBuilder: FormBuilder,
    private router: Router,
    private modalService: NgbModal
  ) {}

  ngOnInit(): void {
    this.bookCopyForm = this.formBuilder.group({
      copyOf: [undefined, Validators.required],
      dateOfEntry: [Date, Validators.required],
      canBeLoaned: [true, Validators.required],
      isCurrentlyLoaned: [false, Validators.required],
      loanedTo: undefined,
    });

    if (this.bookCopy != undefined) {
      this.isAnEdit = true;
      this.editId = this.bookCopy._id;
      this.bookCopyForm.setValue({
        copyOf: this.bookCopy.copyOf,
        dateOfEntry: this.bookCopy.dateOfEntry,
        canBeLoaned: this.bookCopy.canBeLoaned,
        isCurrentlyLoaned: this.bookCopy.isCurrentlyLoaned,
        loanedTo: this.bookCopy.loanedTo,
      });
    }

    if (this.readOnly) {
      this.bookCopyForm.disable();
    }
  }

  submitForm(): void {
    this.loading = true;
    let bookCopy = this.bookCopyForm.value;
    if (!bookCopy.canBeLoaned) {
      bookCopy.isCurrentlyLoaned = false;
      bookCopy.loanedTo = null;
    }
    if (this.isAnEdit) {
      bookCopy._id = this.editId;
      this.updateBookCopy(bookCopy);
    } else {
      this.createBookCopy(bookCopy);
    }
  }

  private updateBookCopy(bookCopy: BookCopy): void {
    this.subscriptions.push(
      this.bookCopyService.updateBookCopy(bookCopy).subscribe((result) => {
        this.router.navigateByUrl(`/bookcopy/details/${result._id}`);
        this.loading = false;
      })
    );
  }

  private createBookCopy(bookCopy: BookCopy): void {
    this.subscriptions.push(
      this.bookCopyService.createBookCopy(bookCopy).subscribe((result) => {
        console.log(result);
        this.router.navigateByUrl(`/bookcopy/details/${result._id}`);
        this.loading = false;
      })
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((item) => item.unsubscribe());
  }

  // Get functions to check for validation easier.
  get copyOf() {
    return this.bookCopyForm.get('copyOf');
  }

  get dateOfEntry() {
    return this.bookCopyForm.get('dateOfEntry');
  }

  get canBeLoaned() {
    return this.bookCopyForm.get('canBeLoaned');
  }

  get isCurrentlyLoaned() {
    return this.bookCopyForm.get('isCurrentlyLoaned');
  }

  // Writers field of form
  setBook(newBook: Book) {
    this.copyOf.setValue(newBook);
  }

  // Modal code
  openModal(content) {
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title', size: 'xl' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookCopyFormComponent } from './book-copy-form.component';

describe('BookCopyFormComponent', () => {
  let component = true;
  let fixture: ComponentFixture<BookCopyFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BookCopyFormComponent],
    }).compileComponents();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

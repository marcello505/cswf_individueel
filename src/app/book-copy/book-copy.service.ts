import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Book } from '../book/book.service';
import { ErrorHandlerService } from '../error-handler.service';

@Injectable({
  providedIn: 'root',
})
export class BookCopyService {
  private bookCopyUrl = environment.apiUrl + '/bookcopy';
  private httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };
  constructor(
    private http: HttpClient,
    private errorHandlerService: ErrorHandlerService
  ) {}

  getAll(): Observable<BookCopy[]> {
    return this.http.get(this.bookCopyUrl).pipe(
      map((rawResult: Array<BookCopy>) => {
        rawResult.forEach((element) => {
          element.dateOfEntry = new Date(element.dateOfEntry);
        });
        return rawResult;
      }),
      catchError(this.errorHandlerService.handleError<BookCopy[]>('getAll', []))
    );
  }

  getById(id: String): Observable<BookCopy> {
    return this.http.get(this.bookCopyUrl + `/${id}`).pipe(
      map((rawResult: BookCopy) => {
        rawResult.dateOfEntry = new Date(rawResult.dateOfEntry);
        return rawResult;
      }),
      catchError(
        this.errorHandlerService.handleError<BookCopy>(`getById id=${id}`)
      )
    );
  }

  createBookCopy(bookCopy: BookCopy): Observable<BookCopy> {
    return this.http.post(this.bookCopyUrl, bookCopy, this.httpOptions).pipe(
      map((rawResult: BookCopy) => {
        rawResult.dateOfEntry = new Date(rawResult.dateOfEntry);
        return rawResult;
      }),
      catchError(
        this.errorHandlerService.handleError<BookCopy>(`createBookCopy`)
      )
    );
  }

  updateBookCopy(bookCopy: BookCopy): Observable<BookCopy> {
    return this.http
      .put(this.bookCopyUrl + `/${bookCopy._id}`, bookCopy, this.httpOptions)
      .pipe(
        map((rawResult: BookCopy) => {
          rawResult.dateOfEntry = new Date(rawResult.dateOfEntry);
          return rawResult;
        }),
        catchError(
          this.errorHandlerService.handleError<BookCopy>(`updateBookCopy`)
        )
      );
  }

  deleteBookCopy(id: String): Observable<BookCopy> {
    return this.http
      .delete<BookCopy>(this.bookCopyUrl + `/${id}`)
      .pipe(
        catchError(
          this.errorHandlerService.handleError<BookCopy>(`deleteBookCopy`)
        )
      );
  }

  loanBookCopy(id: String): Observable<boolean> {
    return this.http
      .post(this.bookCopyUrl + `/loan/${id}`, {}, this.httpOptions)
      .pipe(
        map((rawResult) => {
          if (rawResult['status']) {
            return true;
          } else {
            return false;
          }
        }),
        catchError(
          this.errorHandlerService.handleError<boolean>('loanBookCopy')
        )
      );
  }

  unloanBookCopy(id: String): Observable<boolean> {
    return this.http
      .post(this.bookCopyUrl + `/unloan/${id}`, {}, this.httpOptions)
      .pipe(
        map((rawResult) => {
          if (rawResult['status']) {
            return true;
          } else {
            return false;
          }
        }),
        catchError(
          this.errorHandlerService.handleError<boolean>('loanBookCopy')
        )
      );
  }

  getByBookId(bookId: String): Observable<BookCopy[]> {
    return this.http.get(this.bookCopyUrl + `?copyOf=${bookId}`).pipe(
      map((rawResult: Array<BookCopy>) => {
        rawResult.forEach((element) => {
          element.dateOfEntry = new Date(element.dateOfEntry);
        });
        return rawResult;
      }),
      catchError(
        this.errorHandlerService.handleError<BookCopy[]>('getByBookId', [])
      )
    );
  }

  getLoanedBookCopies(accountId: String): Observable<BookCopy[]> {
    return this.http.get(this.bookCopyUrl + `?loanedTo=${accountId}`).pipe(
      map((rawResult: Array<BookCopy>) => {
        rawResult.forEach((element) => {
          element.dateOfEntry = new Date(element.dateOfEntry);
        });
        return rawResult;
      }),
      catchError(
        this.errorHandlerService.handleError<BookCopy[]>(
          'getLoanedBookCopies',
          []
        )
      )
    );
  }
}

export interface BookCopy {
  _id: String;
  copyOf: Book;
  dateOfEntry: Date;
  canBeLoaned: boolean;
  isCurrentlyLoaned: boolean;
  loanedTo: String;
}

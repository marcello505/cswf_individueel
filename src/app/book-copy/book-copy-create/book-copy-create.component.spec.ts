import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookCopyCreateComponent } from './book-copy-create.component';

describe('BookCopyCreateComponent', () => {
  let component: BookCopyCreateComponent;
  let fixture: ComponentFixture<BookCopyCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BookCopyCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookCopyCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookCopyComponent } from './book-copy.component';

describe('BookCopyComponent', () => {
  let component = true;
  let fixture: ComponentFixture<BookCopyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BookCopyComponent],
    }).compileComponents();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { ErrorHandlerService } from '../error-handler.service';

import { BookCopyService } from './book-copy.service';

describe('BookCopyService', () => {
  let service: BookCopyService;

  beforeEach(() => {
    const httpSpy = jasmine.createSpyObj('HttpClient', [
      'post',
      'get',
      'put',
      'delete',
    ]);
    const errorHandlerSpy = jasmine.createSpyObj('ErrorHandlerService', [
      'handleError',
    ]);
    TestBed.configureTestingModule({
      providers: [
        { provide: HttpClient, useValue: httpSpy },
        { provide: ErrorHandlerService, useValue: errorHandlerSpy },
      ],
    });
    service = TestBed.inject(BookCopyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookCopyListComponent } from './book-copy-list.component';

describe('BookCopyListComponent', () => {
  let component = true;
  let fixture: ComponentFixture<BookCopyListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BookCopyListComponent],
    }).compileComponents();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

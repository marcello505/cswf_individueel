import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Writer } from 'src/app/writer/writer.service';
import { BookCopy, BookCopyService } from '../book-copy.service';

@Component({
  selector: 'app-book-copy-list',
  templateUrl: './book-copy-list.component.html',
  styleUrls: ['./book-copy-list.component.css'],
})
export class BookCopyListComponent implements OnInit, OnDestroy, AfterViewInit {
  private subscriptions: Subscription[] = [];
  @Input() bookCopies: BookCopy[];
  @Output() clickedBookCopy = new EventEmitter<BookCopy>();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  @Input() displayedColumns: string[] = [
    'copyOf',
    'dateOfEntry',
    'canBeLoaned',
  ];
  dataSource: MatTableDataSource<BookCopy> = new MatTableDataSource();

  constructor(
    private bookCopyService: BookCopyService,
    private router: Router
  ) {}

  ngOnInit(): void {
    if (this.bookCopies === undefined) {
      this.subscriptions.push(
        this.bookCopyService.getAll().subscribe((result) => {
          this.bookCopies = result;
          this.dataSource.data = result;
        })
      );
    } else {
      this.dataSource.data = this.bookCopies;
    }
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((element) => element.unsubscribe());
  }

  clickBookCopy(item) {
    this.clickedBookCopy.emit(item);
    if (this.clickedBookCopy.observers.length > 0) {
      this.clickedBookCopy.emit(item);
    } else {
      this.router.navigateByUrl(`/bookcopy/details/${item._id}`);
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}

import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { Book, BookService } from 'src/app/book/book.service';
import { Profile, ProfileService } from '../profile.service';

@Component({
  selector: 'app-profile-form',
  templateUrl: './profile-form.component.html',
  styleUrls: ['./profile-form.component.css'],
})
export class ProfileFormComponent implements OnInit {
  private subscriptions: Subscription[] = [];
  @Input() profile: Profile;
  @Input() readOnly = false;
  profileForm: FormGroup;

  // Modal vars
  closeResult = '';

  // Form state
  isAnEdit = false;
  editId: String;
  loading = false;

  constructor(
    private profileService: ProfileService,
    private bookService: BookService,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private modalService: NgbModal
  ) {}

  ngOnInit(): void {
    this.profileForm = this.formBuilder.group({
      name: ['', Validators.required],
      bio: ['', Validators.required],
      favoriteBooks: this.formBuilder.array([]),
    });

    if (this.profile != undefined) {
      this.isAnEdit = true;
      this.editId = this.profile.accountId;
      this.profileForm.setValue({
        name: this.profile.name,
        bio: this.profile.bio,
        favoriteBooks: [],
      });
      this.profile.favoriteBooks.forEach((element) => this.addBook(element));
    }
    console.log(this.profileForm);

    if (this.readOnly) {
      this.profileForm.disable();
    }
  }

  submitForm(): void {
    this.loading = true;
    let profile = this.profileForm.value;
    let booksIdOnly = [];
    profile.favoriteBooks.forEach((element) => {
      booksIdOnly.push(element._id);
    });
    profile.favoriteBooks = booksIdOnly;
    this.subscriptions.push(
      this.profileService.updateProfile(profile).subscribe((result) => {
        this.router.navigateByUrl(`/profile/details/${result.accountId}`);
        this.loading = false;
      })
    );
  }

  get name() {
    return this.profileForm.get('name');
  }

  get bio() {
    return this.profileForm.get('bio');
  }

  get favoriteBooks() {
    return this.profileForm.get('favoriteBooks') as FormArray;
  }

  addBook(book: Book) {
    const bookFB = this.formBuilder.group({
      _id: [],
      title: [],
    });

    // Check if writer is already here.
    if (
      this.favoriteBooks.value.length === 0 ||
      !this.favoriteBooks.value.find((item) => item._id == book._id)
    ) {
      bookFB.setValue({
        _id: book._id,
        title: book.title,
      });
      this.favoriteBooks.push(bookFB);
    }
  }

  deleteBook(i) {
    this.favoriteBooks.removeAt(i);
  }
  // Modal code
  openModal(content) {
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title', size: 'xl' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}

import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { BookCopy, BookCopyService } from 'src/app/book-copy/book-copy.service';
import { Book, BookService } from 'src/app/book/book.service';
import { NavbarComponent } from 'src/app/navbar/navbar.component';
import { WriterService } from 'src/app/writer/writer.service';
import { Profile, ProfileService } from '../profile.service';

@Component({
  selector: 'app-profile-details',
  templateUrl: './profile-details.component.html',
  styleUrls: ['./profile-details.component.css'],
})
export class ProfileDetailsComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  profile: Profile;
  favoriteBooks: Book[];
  followers: Profile[];
  following: Profile[];
  loanedBookCopies: BookCopy[];

  loggedIn = localStorage.getItem('account_id').length > 0;
  isLoggedInUser = false;
  isFollowingUser = false;

  constructor(
    private profileService: ProfileService,
    private bookCopyService: BookCopyService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    const accountId = this.route.snapshot.paramMap.get('id');
    this.subscriptions.push(
      this.bookCopyService
        .getLoanedBookCopies(accountId)
        .subscribe((result) => {
          this.loanedBookCopies = result;
        })
    );
    this.subscriptions.push(
      this.profileService.getById(accountId).subscribe((result) => {
        this.profile = result;
        if (this.profile == undefined) {
          this.router.navigateByUrl('/404');
        } else {
          this.favoriteBooks = result.favoriteBooks;
          this.followers = result['followers'];
          this.following = result['following'];
          if (this.profile.accountId == localStorage.getItem('account_id')) {
            //Check if this profile page is of the logged in user
            this.isLoggedInUser = true;
          } else {
            //Check if the user is already following the page
            this.followers.filter((element) => {
              if (element.accountId == localStorage.getItem('account_id')) {
                this.isFollowingUser = true;
              }
            });
          }
        }
      })
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  toggleFollow(): void {
    if (this.isFollowingUser) {
      this.subscriptions.push(
        this.profileService
          .unfollowProfile(this.profile.accountId)
          .subscribe((result) => {
            this.isFollowingUser = false;
          })
      );
    } else {
      this.subscriptions.push(
        this.profileService
          .followProfile(this.profile.accountId)
          .subscribe((result) => {
            this.isFollowingUser = true;
          })
      );
    }
  }
}

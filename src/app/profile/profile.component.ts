import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Profile } from './profile.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
})
export class ProfileComponent implements OnInit {
  constructor(private router: Router) {}

  ngOnInit(): void {}

  navigateToProfile(profile: Profile) {
    this.router.navigateByUrl(`/profile/details/${profile.accountId}`);
  }
}

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Book } from '../book/book.service';
import { ErrorHandlerService } from '../error-handler.service';

@Injectable({
  providedIn: 'root',
})
export class ProfileService {
  private profileUrl = environment.apiUrl + '/profile';
  private httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };
  constructor(
    private http: HttpClient,
    private errorHandlerService: ErrorHandlerService
  ) {}

  getAll(): Observable<Profile[]> {
    return this.http.get(this.profileUrl, this.httpOptions).pipe(
      map((rawResult: Array<Profile>) => {
        return rawResult;
      }),
      catchError(this.errorHandlerService.handleError<Profile[]>('getAll', []))
    );
  }

  getById(id: String): Observable<Profile> {
    return this.http.get(this.profileUrl + `/${id}`, this.httpOptions).pipe(
      map((rawResult: Profile) => {
        rawResult.favoriteBooks.forEach((element) => {
          element.releaseDate = new Date(element.releaseDate);
        });
        return rawResult;
      }),
      catchError(
        this.errorHandlerService.handleError<Profile>(`getById id=${id}`)
      )
    );
  }

  updateProfile(profile: Profile): Observable<Profile> {
    return this.http.put(this.profileUrl, profile, this.httpOptions).pipe(
      map((rawResult: Profile) => {
        return rawResult;
      }),
      catchError(this.errorHandlerService.handleError<Profile>('updateProfile'))
    );
  }

  deleteProfile(): Observable<Profile> {
    return this.http
      .delete<Profile>(this.profileUrl, this.httpOptions)
      .pipe(
        catchError(
          this.errorHandlerService.handleError<Profile>(`deleteWriter`)
        )
      );
  }

  followProfile(id: String): Observable<Profile> {
    return this.http
      .post<Profile>(this.profileUrl + `/follow/${id}`, {}, this.httpOptions)
      .pipe(
        catchError(
          this.errorHandlerService.handleError<Profile>('followProfile')
        )
      );
  }

  unfollowProfile(id: String): Observable<Profile> {
    return this.http
      .post<Profile>(this.profileUrl + `/unfollow/${id}`, {}, this.httpOptions)
      .pipe(
        catchError(
          this.errorHandlerService.handleError<Profile>('followProfile')
        )
      );
  }
}

export interface Profile {
  accountId: String;
  name: String;
  bio: String;
  favoriteBooks: Book[];
}

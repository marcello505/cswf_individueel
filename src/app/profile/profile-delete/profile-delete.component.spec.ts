import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileDeleteComponent } from './profile-delete.component';

describe('ProfileDeleteComponent', () => {
  let component = true;
  let fixture: ComponentFixture<ProfileDeleteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ProfileDeleteComponent],
    }).compileComponents();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

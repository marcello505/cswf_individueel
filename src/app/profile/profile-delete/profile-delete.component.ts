import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Profile, ProfileService } from '../profile.service';

@Component({
  selector: 'app-profile-delete',
  templateUrl: './profile-delete.component.html',
  styleUrls: ['./profile-delete.component.css'],
})
export class ProfileDeleteComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  profile: Profile;

  constructor(
    private profileService: ProfileService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    const id = localStorage.getItem('account_id');
    this.subscriptions.push(
      this.profileService.getById(id).subscribe((result) => {
        this.profile = result;
        if (this.profile == undefined) {
          this.router.navigateByUrl('/404');
        }
      })
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  deleteProfile() {
    this.subscriptions.push(
      this.profileService.deleteProfile().subscribe(() => {
        localStorage.setItem('account_token', '');
        localStorage.setItem('account_id', '');
        this.router.navigateByUrl('/profile');
      })
    );
  }

  cancelDelete() {
    this.router.navigateByUrl(`/profile/details/${this.profile.accountId}`);
  }
}

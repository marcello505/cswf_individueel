import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Writer } from './writer.service';

@Component({
  selector: 'app-writer',
  templateUrl: './writer.component.html',
  styleUrls: ['./writer.component.css'],
})
export class WriterComponent implements OnInit {
  constructor(private router: Router) {}

  ngOnInit(): void {}

  navigateToWriter(writer: Writer) {
    this.router.navigateByUrl(`/writer/details/${writer._id}`);
  }
}

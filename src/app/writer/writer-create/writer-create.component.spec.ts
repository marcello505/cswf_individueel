import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WriterCreateComponent } from './writer-create.component';

describe('WriterCreateComponent', () => {
  let component: WriterCreateComponent;
  let fixture: ComponentFixture<WriterCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WriterCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WriterCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

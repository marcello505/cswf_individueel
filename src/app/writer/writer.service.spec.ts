import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { ErrorHandlerService } from '../error-handler.service';

import { WriterService } from './writer.service';

describe('WriterService', () => {
  let service: WriterService;
  let httpServiceSpy: jasmine.SpyObj<HttpClient>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: HttpClient,
          useValue: jasmine.createSpyObj('HttpClient', [
            'post',
            'get',
            'put',
            'delete',
          ]),
        },
        {
          provide: ErrorHandlerService,
          useValue: jasmine.createSpyObj('ErrorHandlerService', [
            'handleError',
          ]),
        },
      ],
    });
    service = TestBed.inject(WriterService);
  });

  beforeEach(() => {
    httpServiceSpy = TestBed.inject(HttpClient) as jasmine.SpyObj<HttpClient>;

    // Set up methods
    httpServiceSpy.get.and.returnValue(of({}));
    httpServiceSpy.post.and.returnValue(of({}));
    httpServiceSpy.put.and.returnValue(of({}));
    httpServiceSpy.delete.and.returnValue(of({}));
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should call get for getting writers', () => {
    service.getAll();
    service.getById('');
    expect(httpServiceSpy.get.calls.count()).toBe(2);
  });

  it('should call post for creating writers', () => {
    service.createWriter({
      _id: null,
      name: null,
      description: null,
      dateOfBirth: null,
    });
    expect(httpServiceSpy.post.calls.count()).toBe(1);
  });
  it('should call put for updating writers', () => {
    service.updateWriter({
      _id: null,
      name: null,
      description: null,
      dateOfBirth: null,
    });
    expect(httpServiceSpy.put.calls.count()).toBe(1);
  });

  it('should call delete for deleting writers', () => {
    service.deleteWriter('');
    expect(httpServiceSpy.delete.calls.count()).toBe(1);
  });

  it("shouldn't call anything other than get for getting writers", () => {
    service.getAll();
    service.getById('');
    expect(httpServiceSpy.post.calls.count()).toBe(0);
    expect(httpServiceSpy.delete.calls.count()).toBe(0);
    expect(httpServiceSpy.put.calls.count()).toBe(0);
  });
});

import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Writer, WriterService } from '../writer.service';

@Component({
  selector: 'app-writer-form',
  templateUrl: './writer-form.component.html',
  styleUrls: ['./writer-form.component.css'],
})
export class WriterFormComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  @Input() writer: Writer;
  @Input() readOnly = false;
  writerForm: FormGroup;

  // Form state
  isAnEdit = false;
  editId: String;
  loading = false;

  constructor(
    private writerService: WriterService,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.writerForm = this.formBuilder.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
      dateOfBirth: ['', Validators.required],
    });

    // If this is an edit, fill all the values.
    if (this.writer != undefined) {
      this.isAnEdit = true;
      this.editId = this.writer._id;
      this.writerForm.setValue({
        name: this.writer.name,
        description: this.writer.description,
        dateOfBirth: this.writer.dateOfBirth,
      });
    }

    if (this.readOnly) {
      this.writerForm.disable();
    }
  }

  submitForm(): void {
    this.loading = true;
    let writer = this.writerForm.value;
    if (this.isAnEdit) {
      writer._id = this.editId;
      this.updateWriter(writer);
    } else {
      this.createWriter(writer);
    }
  }

  private updateWriter(writer: Writer): void {
    this.subscriptions.push(
      this.writerService.updateWriter(writer).subscribe((result) => {
        this.router.navigateByUrl(`/writer/details/${result._id}`);
        this.loading = false;
      })
    );
  }

  private createWriter(writer: Writer): void {
    this.subscriptions.push(
      this.writerService.createWriter(writer).subscribe((result) => {
        console.log(result);
        this.router.navigateByUrl(`/writer/details/${result._id}`);
        this.loading = false;
      })
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((item) => item.unsubscribe());
  }

  // Get functions to check for validation easier.
  get name() {
    return this.writerForm.get('name');
  }

  get description() {
    return this.writerForm.get('description');
  }

  get dateOfBirth() {
    return this.writerForm.get('dateOfBirth');
  }
}

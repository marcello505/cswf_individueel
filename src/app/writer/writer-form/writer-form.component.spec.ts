import { HttpClient } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { ErrorHandlerService } from 'src/app/error-handler.service';
import { Writer, WriterService } from '../writer.service';

import { WriterFormComponent } from './writer-form.component';
import { MockProvider } from 'ng-mocks';
import { ActivatedRoute, Router } from '@angular/router';
import { of } from 'rxjs';

describe('WriterFormComponent', () => {
  let component: WriterFormComponent;
  let fixture: ComponentFixture<WriterFormComponent>;
  let httpServiceSpy: jasmine.SpyObj<HttpClient>;
  let writerServiceSpy: jasmine.SpyObj<WriterService>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [WriterFormComponent],
      providers: [
        FormBuilder,
        MockProvider(Router),
        {
          provide: WriterService,
          useValue: jasmine.createSpyObj('WriterService', [
            'createWriter',
            'updateWriter',
          ]),
        },
        {
          provide: HttpClient,
          useValue: jasmine.createSpyObj('HttpClient', [
            'post',
            'get',
            'put',
            'delete',
          ]),
        },
        {
          provide: ErrorHandlerService,
          useValue: jasmine.createSpyObj('ErrorHandlerService', [
            'handleError',
          ]),
        },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: {
                get: (blah) => {
                  return 'test';
                },
              },
            },
          },
        },
      ],
    }).compileComponents();
  });
  beforeEach(() => {
    httpServiceSpy = TestBed.inject(HttpClient) as jasmine.SpyObj<HttpClient>;
    writerServiceSpy = TestBed.inject(
      WriterService
    ) as jasmine.SpyObj<WriterService>;

    // Set up methods
    httpServiceSpy.get.and.returnValue(of({}));
    httpServiceSpy.post.and.returnValue(of({}));
    httpServiceSpy.put.and.returnValue(of({}));
    httpServiceSpy.delete.and.returnValue(of({}));

    writerServiceSpy.createWriter.and.returnValue(of());
    writerServiceSpy.updateWriter.and.returnValue(of());
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WriterFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call create on default submit', () => {
    component.submitForm();
    expect(writerServiceSpy.createWriter.calls.count()).toBe(1);
  });

  it('should call update on submit when its an edit', () => {
    component.isAnEdit = true;
    component.submitForm();
    expect(writerServiceSpy.updateWriter.calls.count()).toBe(1);
  });

  it("shouldnt call create on submit when it's an edit", () => {
    component.isAnEdit = true;
    component.submitForm();
    expect(writerServiceSpy.createWriter.calls.count()).toBe(0);
  });

  it('shouldnt call update on default submit', () => {
    component.submitForm();
    expect(writerServiceSpy.updateWriter.calls.count()).toBe(0);
  });

  it('should validate the values in the formgroup', () => {
    component.writerForm.setValue({
      name: null,
      description: null,
      dateOfBirth: null,
    });
    expect(component.writerForm.valid).toBe(false);
    component.writerForm.setValue({
      name: 'test',
      description: 'test',
      dateOfBirth: new Date(),
    });
    expect(component.writerForm.valid).toBe(true);
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WriterDeleteComponent } from './writer-delete.component';

describe('WriterDeleteComponent', () => {
  let component = true;
  let fixture: ComponentFixture<WriterDeleteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [WriterDeleteComponent],
    }).compileComponents();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

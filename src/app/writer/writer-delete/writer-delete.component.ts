import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Writer, WriterService } from '../writer.service';

@Component({
  selector: 'app-writer-delete',
  templateUrl: './writer-delete.component.html',
  styleUrls: ['./writer-delete.component.css'],
})
export class WriterDeleteComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  writer: Writer;

  constructor(
    private writerService: WriterService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.subscriptions.push(
      this.writerService.getById(id).subscribe((result) => {
        this.writer = result;
        if (this.writer == undefined) {
          this.router.navigateByUrl('/404');
        }
      })
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  deleteWriter() {
    this.subscriptions.push(
      this.writerService
        .deleteWriter(this.writer._id)
        .subscribe(() => this.router.navigateByUrl('/writer'))
    );
  }

  cancelDelete() {
    this.router.navigateByUrl(`/writer/details/${this.writer._id}`);
  }
}

import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { Writer, WriterService } from '../writer.service';

@Component({
  selector: 'app-writer-edit',
  templateUrl: './writer-edit.component.html',
  styleUrls: ['./writer-edit.component.css'],
})
export class WriterEditComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  writer: Writer;

  constructor(
    private writerService: WriterService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.subscriptions.push(
      this.writerService
        .getById(id)
        .subscribe((result) => (this.writer = result))
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WriterEditComponent } from './writer-edit.component';

describe('WriterEditComponent', () => {
  let component = true;
  let fixture: ComponentFixture<WriterEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [WriterEditComponent],
    }).compileComponents();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

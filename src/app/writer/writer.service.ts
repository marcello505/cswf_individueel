import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ErrorHandlerService } from '../error-handler.service';

@Injectable({
  providedIn: 'root',
})
export class WriterService {
  private writerUrl = environment.apiUrl + '/writer';
  private httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };
  constructor(
    private http: HttpClient,
    private errorHandlerService: ErrorHandlerService
  ) {}

  getAll(): Observable<Writer[]> {
    return this.http.get(this.writerUrl).pipe(
      map((rawResult: Array<Writer>) => {
        rawResult.forEach((element) => {
          element.dateOfBirth = new Date(element.dateOfBirth);
        });
        return rawResult;
      }),
      catchError(this.errorHandlerService.handleError<Writer[]>('getAll', []))
    );
  }

  getById(id: String): Observable<Writer> {
    return this.http.get(this.writerUrl + `/${id}`).pipe(
      map((rawResult: Writer) => {
        rawResult.dateOfBirth = new Date(rawResult.dateOfBirth);
        return rawResult;
      }),
      catchError(
        this.errorHandlerService.handleError<Writer>(`getById id=${id}`)
      )
    );
  }

  createWriter(writer: Writer): Observable<Writer> {
    return this.http.post(this.writerUrl, writer, this.httpOptions).pipe(
      map((rawResult: Writer) => {
        rawResult.dateOfBirth = new Date(rawResult.dateOfBirth);
        return rawResult;
      }),
      catchError(this.errorHandlerService.handleError<Writer>(`createWriter`))
    );
  }

  updateWriter(writer: Writer): Observable<Writer> {
    return this.http
      .put(this.writerUrl + `/${writer._id}`, writer, this.httpOptions)
      .pipe(
        map((rawResult: Writer) => {
          rawResult.dateOfBirth = new Date(rawResult.dateOfBirth);
          return rawResult;
        }),
        catchError(this.errorHandlerService.handleError<Writer>(`updateWriter`))
      );
  }

  deleteWriter(id: String): Observable<Writer> {
    return this.http
      .delete<Writer>(this.writerUrl + `/${id}`)
      .pipe(
        catchError(this.errorHandlerService.handleError<Writer>(`deleteWriter`))
      );
  }
}

export interface Writer {
  _id: String;
  name: String;
  description: String;
  dateOfBirth: Date;
}

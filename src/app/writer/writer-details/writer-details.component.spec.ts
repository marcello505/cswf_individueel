import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WriterDetailsComponent } from './writer-details.component';

describe('WriterDetailsComponent', () => {
  let component = true;
  let fixture: ComponentFixture<WriterDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [WriterDetailsComponent],
    }).compileComponents();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

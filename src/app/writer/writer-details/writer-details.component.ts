import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Book, BookService } from 'src/app/book/book.service';
import { Writer, WriterService } from '../writer.service';

@Component({
  selector: 'app-writer-details',
  templateUrl: './writer-details.component.html',
  styleUrls: ['./writer-details.component.css'],
})
export class WriterDetailsComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  writer: Writer;
  booksByWriter: Book[];

  constructor(
    private writerService: WriterService,
    private bookService: BookService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    const writerId = this.route.snapshot.paramMap.get('id');
    this.subscriptions.push(
      this.bookService.getBooksByWriter(writerId).subscribe((result) => {
        this.booksByWriter = result;
      })
    );
    this.subscriptions.push(
      this.writerService.getById(writerId).subscribe((result) => {
        this.writer = result;
        if (this.writer == undefined) {
          this.router.navigateByUrl('/404');
        }
      })
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}

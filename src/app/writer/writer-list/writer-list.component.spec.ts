import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WriterListComponent } from './writer-list.component';

describe('WriterListComponent', () => {
  let component = true;
  let fixture: ComponentFixture<WriterListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [WriterListComponent],
    }).compileComponents();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

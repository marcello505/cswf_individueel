import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Subscription } from 'rxjs';
import { Writer, WriterService } from '../writer.service';

@Component({
  selector: 'app-writer-list',
  templateUrl: './writer-list.component.html',
  styleUrls: ['./writer-list.component.css'],
})
export class WriterListComponent implements OnInit, OnDestroy, AfterViewInit {
  private subscriptions: Subscription[] = [];
  @Input() writers: Writer[];
  @Output() clickedWriter = new EventEmitter<Writer>();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  @Input() displayedColumns: string[] = ['name', 'dateOfBirth'];
  dataSource: MatTableDataSource<Writer> = new MatTableDataSource();

  constructor(private writerService: WriterService) {}

  ngOnInit(): void {
    if (this.writers === undefined) {
      this.subscriptions.push(
        this.writerService.getAll().subscribe((result) => {
          this.writers = result;
          this.dataSource.data = result;
        })
      );
    } else {
      this.dataSource.data = this.writers;
    }
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((element) => element.unsubscribe());
  }

  clickWriter(item) {
    console.log(item);
    this.clickedWriter.emit(item);
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}

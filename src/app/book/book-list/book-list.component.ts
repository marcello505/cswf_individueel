import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { BookService, Book } from '../book.service';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css'],
})
export class BookListComponent implements OnInit, AfterViewInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  @Input() books: Book[];
  @Output() clickedBook = new EventEmitter<Book>();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  @Input() displayedColumns: string[] = [
    'title',
    'isbn',
    'writer',
    'releaseDate',
  ];
  dataSource: MatTableDataSource<Book> = new MatTableDataSource();

  constructor(private bookService: BookService, private router: Router) {}

  ngOnInit(): void {
    if (this.books === undefined) {
      this.subscriptions.push(
        this.bookService
          .getAll()
          .subscribe((result) => (this.dataSource.data = result))
      );
    } else {
      this.dataSource.data = this.books;
    }
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((item) => item.unsubscribe());
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  clickBook(item) {
    if (this.clickedBook.observers.length > 0) {
      this.clickedBook.emit(item);
    } else {
      this.router.navigateByUrl(`/book/details/${item._id}`);
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}

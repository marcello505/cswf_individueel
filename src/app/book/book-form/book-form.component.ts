import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { BookService, Book } from '../book.service';
import { Writer } from '../../writer/writer.service';

@Component({
  selector: 'app-book-form',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.css'],
})
export class BookFormComponent implements OnInit, OnDestroy {
  @Input() book: Book;
  @Input() readOnly = false;
  bookForm: FormGroup;
  private subscriptions: Subscription[] = [];

  // Modal vars
  closeResult = '';

  // Form state
  isAnEdit = false;
  editId: String;
  loading = false;

  constructor(
    private bookService: BookService,
    private formBuilder: FormBuilder,
    private router: Router,
    private modalService: NgbModal
  ) {}

  ngOnInit(): void {
    this.bookForm = this.formBuilder.group({
      title: ['', Validators.required],
      ISBN: [
        '',
        [
          Validators.required,
          Validators.maxLength(13),
          Validators.minLength(10),
        ],
      ],
      description: ['', Validators.required],
      releaseDate: [Date, Validators.required],
      writer: [undefined, Validators.required],
      publisher: ['', Validators.required],
      language: ['', Validators.required],
    });

    // If this is an edit, fill all the values.
    if (this.book != undefined) {
      this.isAnEdit = true;
      this.editId = this.book._id;
      this.bookForm.setValue({
        ISBN: this.book.ISBN,
        title: this.book.title,
        description: this.book.description,
        releaseDate: this.book.releaseDate,
        writer: this.book.writer,
        publisher: this.book.publisher,
        language: this.book.language,
      });
    }

    if (this.readOnly) {
      this.bookForm.disable();
    }
  }

  submitForm(): void {
    this.loading = true;
    console.log('submitForm has been called');
    let book = this.bookForm.value;
    if (this.isAnEdit) {
      book._id = this.editId;
      this.updateBook(book);
    } else {
      this.createBook(book);
    }
  }

  private updateBook(book: Book): void {
    this.subscriptions.push(
      this.bookService.updateBook(book).subscribe((result) => {
        this.router.navigateByUrl('/book/details/' + result._id);
        this.loading = false;
      })
    );
  }
  private createBook(book: Book): void {
    this.subscriptions.push(
      this.bookService.createBook(book).subscribe((result) => {
        console.log(result);
        this.router.navigateByUrl('/book/details/' + result._id);
        this.loading = false;
      })
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((item) => item.unsubscribe());
  }

  // Get functions to check for validation easier.
  get title() {
    return this.bookForm.get('title');
  }
  get ISBN() {
    return this.bookForm.get('ISBN');
  }
  get description() {
    return this.bookForm.get('description');
  }
  Person;
  get releaseDate() {
    return this.bookForm.get('releaseDate');
  }

  get writer() {
    return this.bookForm.get('writer');
  }

  get publisher() {
    return this.bookForm.get('publisher');
  }

  get language() {
    return this.bookForm.get('language');
  }

  // Writers field of form
  setWriter(newWriter: Writer) {
    this.writer.setValue(newWriter);
  }

  deleteWriter(i) {
    this.writer.setValue(undefined);
  }

  // Modal code
  openModal(content) {
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title', size: 'xl' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookDeleteComponent } from './book-delete.component';
import { MockModule, MockProvider } from 'ng-mocks';
import { HttpClient } from '@angular/common/http';
import { ErrorHandlerService } from 'src/app/error-handler.service';
import { BookService } from '../book.service';
import { ActivatedRoute, Router } from '@angular/router';
import { of } from 'rxjs';

describe('BookDeleteComponent', () => {
  let component: BookDeleteComponent;
  let fixture: ComponentFixture<BookDeleteComponent>;
  let httpServiceSpy: jasmine.SpyObj<HttpClient>;
  let bookServiceSpy: jasmine.SpyObj<BookService>;

  beforeEach(async () => {
    const httpSpy = jasmine.createSpyObj('HttpClient', [
      'post',
      'get',
      'put',
      'delete',
    ]);
    const errorHandlerSpy = jasmine.createSpyObj('ErrorHandlerService', [
      'handleError',
    ]);
    await TestBed.configureTestingModule({
      declarations: [BookDeleteComponent],
      providers: [
        { provide: HttpClient, useValue: httpSpy },
        { provide: ErrorHandlerService, useValue: errorHandlerSpy },
        MockProvider(BookService),
        MockProvider(Router),
        MockProvider(ActivatedRoute),
        {
          provide: BookService,
          useValue: jasmine.createSpyObj('BookService', ['getById']),
        },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: {
                get: (blah) => {
                  return 'test';
                },
              },
            },
          },
        },
      ],
    }).compileComponents();
    httpServiceSpy = TestBed.inject(HttpClient) as jasmine.SpyObj<HttpClient>;
  });

  beforeEach(() => {
    //Inject dependencies
    bookServiceSpy = TestBed.inject(BookService) as jasmine.SpyObj<BookService>;

    //Set up methods
    bookServiceSpy.getById.and.returnValue(
      of({
        _id: null,
        title: null,
        ISBN: null,
        description: null,
        releaseDate: null,
        writer: null,
        language: null,
        publisher: null,
      })
    );
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

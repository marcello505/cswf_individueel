import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { BookService, Book } from '../book.service';

@Component({
  selector: 'app-book-delete',
  templateUrl: './book-delete.component.html',
  styleUrls: ['./book-delete.component.css'],
})
export class BookDeleteComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  book: Book;

  constructor(
    private bookService: BookService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.subscriptions.push(
      this.bookService.getById(id).subscribe((result) => {
        this.book = result;
        if (this.book == undefined) {
          this.router.navigateByUrl('/404');
        }
      })
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  deleteBook() {
    this.bookService
      .deleteBook(this.book._id)
      .subscribe((_) => this.router.navigateByUrl('/book'));
  }

  cancelDelete() {
    this.router.navigateByUrl('/book/details/' + this.book._id);
  }
}

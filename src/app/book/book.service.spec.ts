import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { MockModule, MockProvider } from 'ng-mocks';
import { ErrorHandlerService } from '../error-handler.service';

import { BookService } from './book.service';

describe('BookService', () => {
  let service: BookService;
  let httpServiceSpy: jasmine.SpyObj<HttpClient>;

  beforeEach(() => {
    const httpSpy = jasmine.createSpyObj('HttpClient', [
      'post',
      'get',
      'put',
      'delete',
    ]);
    const errorHandlerSpy = jasmine.createSpyObj('ErrorHandlerService', [
      'handleError',
    ]);
    TestBed.configureTestingModule({
      providers: [
        { provide: HttpClient, useValue: httpSpy },
        { provide: ErrorHandlerService, useValue: errorHandlerSpy },
      ],
    });
    service = TestBed.inject(BookService);
    httpServiceSpy = TestBed.inject(HttpClient) as jasmine.SpyObj<HttpClient>;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

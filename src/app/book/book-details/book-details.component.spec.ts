import { HttpClient } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { MockModule, MockProvider } from 'ng-mocks';
import { of } from 'rxjs';
import { BookCopyService } from 'src/app/book-copy/book-copy.service';
import { ErrorHandlerService } from 'src/app/error-handler.service';
import { BookService } from '../book.service';

import { BookDetailsComponent } from './book-details.component';

describe('BookDetailsComponent', () => {
  let component: BookDetailsComponent;
  let fixture: ComponentFixture<BookDetailsComponent>;
  let bookServiceSpy: jasmine.SpyObj<BookService>;
  let bookCopyServiceSpy: jasmine.SpyObj<BookCopyService>;
  const httpSpy = jasmine.createSpyObj('HttpClient', [
    'post',
    'get',
    'put',
    'delete',
  ]);
  const errorHandlerSpy = jasmine.createSpyObj('ErrorHandlerService', [
    'handleError',
  ]);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BookDetailsComponent],
      providers: [
        { provide: HttpClient, useValue: httpSpy },
        { provide: ErrorHandlerService, useValue: errorHandlerSpy },
        MockProvider(Router),
        MockProvider(ActivatedRoute),
        {
          provide: BookService,
          useValue: jasmine.createSpyObj('BookService', ['getById']),
        },
        {
          provide: BookCopyService,
          useValue: jasmine.createSpyObj('BookCopyService', ['getByBookId']),
        },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: {
                get: (blah) => {
                  return 'test';
                },
              },
            },
          },
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    //Inject dependencies
    bookServiceSpy = TestBed.inject(BookService) as jasmine.SpyObj<BookService>;
    bookCopyServiceSpy = TestBed.inject(
      BookCopyService
    ) as jasmine.SpyObj<BookCopyService>;

    //Set up methods
    bookCopyServiceSpy.getByBookId.and.returnValue(of([]));
    bookServiceSpy.getById.and.returnValue(
      of({
        _id: 'blah',
        title: null,
        ISBN: null,
        description: null,
        releaseDate: null,
        writer: null,
        language: null,
        publisher: null,
      })
    );
  });

  it('should create', () => {
    expect(true).toBeTruthy();
  });
});

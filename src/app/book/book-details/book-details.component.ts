import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { BookCopy, BookCopyService } from 'src/app/book-copy/book-copy.service';
import { BookService, Book } from '../book.service';

@Component({
  selector: 'app-book-details',
  templateUrl: './book-details.component.html',
  styleUrls: ['./book-details.component.css'],
})
export class BookDetailsComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  book: Book;
  bookCopies: BookCopy[];

  constructor(
    private bookService: BookService,
    private bookCopyService: BookCopyService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    const bookId = this.route.snapshot.paramMap.get('id');
    this.subscriptions.push(
      this.bookCopyService
        .getByBookId(bookId)
        .subscribe((result) => (this.bookCopies = result))
    );
    this.subscriptions.push(
      this.bookService.getById(bookId).subscribe((result) => {
        this.book = result;
        if (this.book == undefined) {
          this.router.navigateByUrl('/404');
        }
      })
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}

import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ErrorHandlerService } from '../error-handler.service';
import { Writer } from '../writer/writer.service';

@Injectable({
  providedIn: 'root',
})
export class BookService {
  private bookUrl = environment.apiUrl + '/book';
  private httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };

  constructor(
    private http: HttpClient,
    private errorHandlerService: ErrorHandlerService
  ) {}

  getAll(): Observable<Book[]> {
    return this.http.get(this.bookUrl).pipe(
      map((rawResult: Array<Book>) => {
        rawResult.forEach((element) => {
          element.releaseDate = new Date(element.releaseDate);
        });

        return rawResult;
      }),
      catchError(this.errorHandlerService.handleError<Book[]>('getAll', []))
    );
  }

  getById(id: String): Observable<Book> {
    return this.http.get(this.bookUrl + '/' + id).pipe(
      map((rawResult: Book) => {
        rawResult.releaseDate = new Date(rawResult.releaseDate);
        return rawResult;
      }),
      catchError(this.errorHandlerService.handleError<Book>(`getById id=${id}`))
    );
  }

  createBook(book: Book): Observable<Book> {
    return this.http.post(this.bookUrl, book, this.httpOptions).pipe(
      map((rawResult: Book) => {
        rawResult.releaseDate = new Date(rawResult.releaseDate);
        return rawResult;
      })
    );
  }

  updateBook(book: Book): Observable<Book> {
    return this.http
      .put(this.bookUrl + '/' + book._id, book, this.httpOptions)
      .pipe(
        map((rawResult: Book) => {
          rawResult.releaseDate = new Date(rawResult.releaseDate);
          return rawResult;
        })
      );
  }

  deleteBook(id: String): Observable<Book> {
    return this.http.delete<Book>(this.bookUrl + '/' + id);
  }

  getBooksByWriter(writerId: String): Observable<Book[]> {
    return this.http.get(this.bookUrl + `?writer=${writerId}`).pipe(
      map((rawResult: Array<Book>) => {
        rawResult.forEach((element) => {
          element.releaseDate = new Date(element.releaseDate);
        });

        return rawResult;
      }),
      catchError(
        this.errorHandlerService.handleError<Book[]>('getBooksByWriter', [])
      )
    );
  }
}

export interface Book {
  _id: String;
  title: String;
  ISBN: String;
  description: String;
  releaseDate: Date;
  writer: Writer;
  publisher: String;
  language: String;
}

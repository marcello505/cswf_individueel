import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Book } from './book.service';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css'],
})
export class BookComponent implements OnInit {
  constructor(private router: Router) {}

  ngOnInit(): void {}

  navigateToBook(book: Book) {
    this.router.navigateByUrl('/book/details/' + book._id);
  }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { AccountGuard } from './auth/account.guard';
import { LoginComponent } from './auth/login/login.component';
import { LogoutComponent } from './auth/logout/logout.component';
import { RegisterComponent } from './auth/register/register.component';
import { BookCopyCreateComponent } from './book-copy/book-copy-create/book-copy-create.component';
import { BookCopyDeleteComponent } from './book-copy/book-copy-delete/book-copy-delete.component';
import { BookCopyDetailsComponent } from './book-copy/book-copy-details/book-copy-details.component';
import { BookCopyEditComponent } from './book-copy/book-copy-edit/book-copy-edit.component';
import { BookCopyComponent } from './book-copy/book-copy.component';
import { BookCreateComponent } from './book/book-create/book-create.component';
import { BookDeleteComponent } from './book/book-delete/book-delete.component';
import { BookDetailsComponent } from './book/book-details/book-details.component';
import { BookEditComponent } from './book/book-edit/book-edit.component';
import { BookComponent } from './book/book.component';
import { ErrorComponent } from './error/error.component';
import { HomeComponent } from './home/home.component';
import { ProfileDeleteComponent } from './profile/profile-delete/profile-delete.component';
import { ProfileDetailsComponent } from './profile/profile-details/profile-details.component';
import { ProfileEditComponent } from './profile/profile-edit/profile-edit.component';
import { ProfileComponent } from './profile/profile.component';
import { WriterCreateComponent } from './writer/writer-create/writer-create.component';
import { WriterDeleteComponent } from './writer/writer-delete/writer-delete.component';
import { WriterDetailsComponent } from './writer/writer-details/writer-details.component';
import { WriterEditComponent } from './writer/writer-edit/writer-edit.component';
import { WriterComponent } from './writer/writer.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'about', component: AboutComponent },
  {
    path: 'auth',
    children: [
      { path: 'login', component: LoginComponent },
      { path: 'register', component: RegisterComponent },
      { path: 'logout', component: LogoutComponent },
    ],
  },
  {
    path: 'bookcopy',
    children: [
      {
        path: 'delete/:id',
        canActivate: [AccountGuard],
        component: BookCopyDeleteComponent,
      },
      {
        path: 'edit/:id',
        canActivate: [AccountGuard],
        component: BookCopyEditComponent,
      },
      {
        path: 'create',
        canActivate: [AccountGuard],
        component: BookCopyCreateComponent,
      },
      { path: 'details/:id', component: BookCopyDetailsComponent },
      { path: '', pathMatch: 'full', component: BookCopyComponent },
    ],
  },
  {
    path: 'book',
    children: [
      { path: 'details/:id', component: BookDetailsComponent },
      {
        path: 'edit/:id',
        canActivate: [AccountGuard],
        component: BookEditComponent,
      },
      {
        path: 'delete/:id',
        canActivate: [AccountGuard],
        component: BookDeleteComponent,
      },
      {
        path: 'create',
        canActivate: [AccountGuard],
        component: BookCreateComponent,
      },
      { path: '', pathMatch: 'full', component: BookComponent },
    ],
  },
  {
    path: 'writer',
    children: [
      { path: 'details/:id', component: WriterDetailsComponent },
      {
        path: 'edit/:id',
        canActivate: [AccountGuard],
        component: WriterEditComponent,
      },
      {
        path: 'delete/:id',
        canActivate: [AccountGuard],
        component: WriterDeleteComponent,
      },
      {
        path: 'create',
        canActivate: [AccountGuard],
        component: WriterCreateComponent,
      },
      { path: '', pathMatch: 'full', component: WriterComponent },
    ],
  },
  {
    path: 'profile',
    children: [
      {
        path: 'delete',
        canActivate: [AccountGuard],
        component: ProfileDeleteComponent,
      },
      {
        path: 'edit',
        canActivate: [AccountGuard],
        component: ProfileEditComponent,
      },
      { path: 'details/:id', component: ProfileDetailsComponent },
      { path: '', pathMatch: 'full', component: ProfileComponent },
    ],
  },
  { path: '404', component: ErrorComponent },
  { path: '', pathMatch: 'full', redirectTo: '/home' },
  { path: '**', redirectTo: '/404' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}

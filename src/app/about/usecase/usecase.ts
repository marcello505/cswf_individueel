export interface Usecase {
  naam: String;
  beschrijving: String;
  actor: String;
  preConditie: String;
  scenario: String[];
  postConditie: String;
}

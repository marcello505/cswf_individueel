import { Component, OnInit } from '@angular/core';
import { Usecase } from './usecase';

@Component({
  selector: 'app-usecase',
  templateUrl: './usecase.component.html',
  styleUrls: ['./usecase.component.css'],
})
export class UsecaseComponent implements OnInit {
  useCases: Usecase[];

  constructor() {}

  ngOnInit(): void {
    this.useCases = [
      {
        naam: 'UC-01 Account aanmaken en inloggen',
        beschrijving: 'Het aanmaken van een account en inloggen.',
        actor: 'Reguliere gebruiker',
        preConditie: 'Geen',
        scenario: [
          'Gebruiker klikt op de Register knop rechtsbovenin',
          'Gebruiker vult email en password in die hij wilt gebruiken',
          'Als de gegevens correct zijn wordt de gebruiker redirected naar de inlog pagina',
          'Gebruiker vult email en password in en klikt op Login knop.',
          'De applicatie valideert de ingevoerde gegevens.',
          'Indien gegevens correct zijn dan redirect de applicatie naar zijn profiel pagina',
          'gebruiker heeft nu ook de optie rechtsbovenin om uit te loggen',
        ],
        postConditie: 'De actor is ingelogd',
      },
      {
        naam: 'UC-02 Een Boek exemplaar lenen',
        beschrijving: 'Een gebruiker kan een boek lenen',
        actor: 'Reguliere gebruiker',
        preConditie: 'Gebruiker is ingelogd',
        scenario: [
          'Gebruiker navigeert naar de book pagina of de book copy pagina.',
          'De gebruiker zoekt naar een boek via de zoekbalk.',
          'Als de gebruiker een boek vindt dan kan hij erop klikken om de details ervan te zien',
          'Mocht hij bij de book pagina zijn in plaats van book copy pagina dan kan hij onderaan de pagina alle exemplaren van het boek zien. Hier kan hij op eentje klikken die beschikbaar is',
          'De gebruiker kan nu op de leen knop drukken. Als hij niet geleend kan worden dan is de knop grijs, en is de tekst anders.',
          'De gebruiker heeft op lenen gedrukt en nu staat het boek onder zijn naam, hij kan met dezelfde knop het boek ook weer inleveren wanneer hij klaar is.',
        ],
        postConditie: 'De gebruiker heeft het boek geleend',
      },
      {
        naam: 'UC-03 Een Boek in het systeem zetten',
        beschrijving: 'Een medewerker wil een boek in het systeem zetten',
        actor: 'Bibliotheek Medewerker',
        preConditie: 'Medewerker is ingelogd',
        scenario: [
          'De medewerker wil een boek invoeren van een nieuwe schrijver',
          'Sinds de schrijver nog niet in het systeem staat navigeert de medewerker naar writer pagina',
          'De medewerker klikt op de create writer knop en maakt een nieuwe writer aan op de pagina die volgt',
          'De applicatie valideert automatisch alle gegevens en laat alleen het opslaan toe wanneer alles correct is',
          'Met de writer aangemaakt kan de medewerker hetzelfde doen voor het boek zelf. De medewerker navigeert naar de boek pagine',
          'De medewerker klikt op de create book knop en vult de gegevens in. Net als daarvoor wordt alles gevalideert',
          'Bij het maken van een boek moet de gebruiker ook de schrijver kiezen. De medewerker kiest de schrijver die ze net heeft ingevoerd',
          'Nadat alles is ingevoerd en gevalideerd wordt het boek opgeslagen en wordt de medewerker automatisch gestuurd naar de pagina van het boek.',
        ],
        postConditie:
          'Het nieuwe boek samen met de schrijver is ingevoerd in het systeem',
      },
      {
        naam: 'UC-04 Gebruikers volgen',
        beschrijving: 'Een gebruiker wil een andere gebruiker volgen',
        actor: 'Reguliere gebruiker',
        preConditie: 'Gebruiker is ingelogd',
        scenario: [
          'Een vriend van de gebruiker heeft pasgeleden een account gemaakt en de gebruiker wil hem graag volgen',
          'De gebruiker navigeert naar de profile pagina',
          'De gebruiker zoekt de gebruikersnaam op van zijn vriend in de zoekbalk',
          'Wanneer hij tevoorschijn komt klikt de gebruiker op het profiel van zijn vriend',
          "Nu is hij op de pagina van zijn vriend, er staat een duidelijke 'follow' knop bovenaan",
          'De gebruiker klikt erop en volgt zijn vriend, nu ziet hij ook de favoriete boeken van zijn vriend tevoorschijn komen',
        ],
        postConditie: 'De gebruiker heeft zijn vriend gevolgd',
      },
    ];
  }
}

//
// This server will be started by Protractor in end-to-end tests.
// Add your API mocks for your specific project in this file.
//
const express = require("express");
const port = 3000;

let app = express();
const cors = require("cors")
let routes = express.Router();


// Add CORS headers so our external Angular app is allowed to connect
app.use(function (req, res, next) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader(
        "Access-Control-Allow-Methods",
        "GET, POST, OPTIONS, PUT, PATCH, DELETE"
        );
        res.setHeader(
            "Access-Control-Allow-Headers",
            "X-Requested-With,content-type"
            );
            res.setHeader("Access-Control-Allow-Credentials", true);
            next();
        });
app.use(express.json())
app.use(cors())
        
routes.post("/auth/login", (req, res, next) => {
  res.status(200).json({
      stauts: "Login successful",
    token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50SWQiOiI2MDcyY2NiMmY1NGRiZTViNzVmNTdiNDgiLCJpYXQiOjE2MTgyNTM2NTMsImV4cCI6MTYxODQyNjQ1M30.WKAw9vlT3IxYYdWDMgx2t230yryDWsXC_1Ckz4ERL2s",
    account: {
        _id: "6072ccb2f54dbe5b75f57b48",
        email: "test@user.com"
    }
  });
});

routes.get("/profile/:id", (req, res, next) => {
  res.status(200).json({
    favoriteBooks: [
        {
            _id: "60721786e20308910f5bbadc",
            title: "Green eggs and ham",
            ISBN: "1321233122",
            description: "Famous childrens book by Dr.Seuss",
            releaseDate: "1998-11-29T23:00:00.000Z",
            publisher: "Penguin Books",
            language: "English",
            writer: {
                _id: "607223b2d837cee632a16204",
                name: "Dr.Seuss",
                description: "Famous writer of children's books",
                dateOfBirth: "2021-04-12T22:00:00.000Z",
                __v: 0
            },
            __v: 0
        }
    ],
    name: "Marcello",
    accountId: "6072ccb2f54dbe5b75f57b48",
    bio: "Ik hou van mystery boeken",
    followers: [],
    following: [
        {
            favoriteBooks: [],
            name: "test2@user.com",
            accountId: "6072f5d4f54dbe5b75f57b5b",
            bio: ""
        }
    ],
  });
})

routes.get("/auth/verify", (req, res, next)=>{
    res.status(200).json({
    verified: true,
    account: {
        _id: "6072ccb2f54dbe5b75f57b48",
        email: "test@user.com"
    }
    })
})

routes.post("/writer", (req, res, next)=> {
    res.status(201).json({
        _id: "607185d003e9e02572e1eee6",
        name: "Henry Ford",
        description: "blah",
        dateOfBirth: "1916-10-12T23:40:28.000Z",
        __v: 0
    })
})

routes.get("/writer/:id", (req, res, next)=> {
    res.status(200).json({
        _id: "607185d003e9e02572e1eee6",
        name: "Henry Ford",
        description: "blah",
        dateOfBirth: "1916-10-12T23:40:28.000Z",
        __v: 0
    })
})

routes.get("/writer", (req, res, next)=> {
    res.status(200).json([])
})


routes.get("/bookcopy", (req, res, next)=> {
    res.status(200).json([])
})

routes.get("/book/:id", (req, res, next)=>{
    res.status(200).json({
        
        _id: "60721786e20308910f5bbadc",
        title: "Green eggs and ham",
        ISBN: "1321233122",
        description: "Famous childrens book by Dr.Seuss",
        releaseDate: "1998-11-29T23:00:00.000Z",
        publisher: "Penguin Books",
        language: "English",
        writer: {
            _id: "607223b2d837cee632a16204",
            name: "Dr.Seuss",
            description: "Famous writer of children's books",
            dateOfBirth: "2021-04-12T22:00:00.000Z",
            __v: 0
        },
        __v: 0
    })
})

routes.get("/book", (req, res, next)=>{
    res.status(200).json([{
        
        _id: "60721786e20308910f5bbadc",
        title: "Green eggs and ham",
        ISBN: "1321233122",
        description: "Famous childrens book by Dr.Seuss",
        releaseDate: "1998-11-29T23:00:00.000Z",
        publisher: "Penguin Books",
        language: "English",
        writer: {
            _id: "607223b2d837cee632a16204",
            name: "Dr.Seuss",
            description: "Famous writer of children's books",
            dateOfBirth: "2021-04-12T22:00:00.000Z",
            __v: 0
        },
        __v: 0
    }])
})

//
// Write your own mocking API endpoints here.
//

// Finally add your routes to the app
app.use(routes);

app.use("*", function (req, res, next) {
  next({ error: "Non-existing endpoint" });
});

app.use((err, req, res, next) => {
  res.status(400).json(err);
});

app.listen(port, () => {
  console.log("Mock backend server running on port", port);
})

import { AppPage } from './app.po';
import { browser, by, element, logging } from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should make a writer', async () => {
    await page.navigateToLogin();
    await browser.sleep(500);
    await page.login();
    await browser.sleep(500);
    await page.navigateToWriter();
    await element(by.id('create-button')).click();
    await browser.sleep(500);
    await page.fillWriterForm();
    await browser.sleep(500);
    const result = await element(by.id('writer-title')).getText();
    expect(result).toBe('Henry Ford');
  });

  it('should be able to find a book', async () => {
    await page.navigateToBook();
    await browser.sleep(500);
    await element(by.id('filter-field')).sendKeys('Green eggs and ham');
    await browser.sleep(1000);
    await element(by.className('mat-row')).click();
    await browser.sleep(500);
    const result = await element(by.id('book-title')).getText();
    expect(result).toBe('Green eggs and ham');
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(
      jasmine.objectContaining({
        level: logging.Level.SEVERE,
      } as logging.Entry)
    );
  });
});

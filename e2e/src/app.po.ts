import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl) as Promise<unknown>;
  }
  navigateToLogin(): Promise<unknown> {
    return browser.get(browser.baseUrl + `/auth/login`) as Promise<unknown>;
  }
  navigateToWriter(): Promise<unknown> {
    return browser.get(browser.baseUrl + `/writer`) as Promise<unknown>;
  }
  navigateToBook(): Promise<unknown> {
    return browser.get(browser.baseUrl + `/book`) as Promise<unknown>;
  }

  login(): Promise<unknown> {
    return element(by.id('email-field'))
      .sendKeys('test@user.com')
      .then((_) => element(by.id('password-field')).sendKeys('password'))
      .then((_) => element(by.id('login-button')).click()) as Promise<unknown>;
  }

  fillWriterForm(): Promise<unknown> {
    return element(by.id('name-field'))
      .sendKeys('Henry Ford')
      .then((_) =>
        element(by.id('dateOfBirth-field')).sendKeys(new Date().toString())
      )
      .then((_) =>
        element(by.id('description-field')).sendKeys(
          'This is a protractor test.'
        )
      )
      .then((_) => element(by.id('save-button')).click()) as Promise<unknown>;
  }

  getTitleText(): Promise<string> {
    return element(
      by.css('app-root .content span')
    ).getText() as Promise<string>;
  }
}
